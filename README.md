# Bevy Renet Example

This is an example where when you press the spacebar on the client window,
it sends a ping to the server. When the server gets a ping, it sends back a pong.

It is recommended to look at the regular [Renet example](https://gitlab.com/examples-rs/renet-example) before this one.



# How To Use

Server: `cargo run --bin server -- --ip <IP TO LISTEN ON (SERVER IP)> --max <MAX CLIENTS ALLOWED TO CONNECT>`

Client: `cargo run --bin client -- --server <SERVER IP>`

On the client window, press spacebar to send a ping to the server.
