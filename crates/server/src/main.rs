mod cli;

use std::net::*;
use std::time::*;
use bevy::prelude::*;
use bevy_renet::*;
use bevy_renet::renet::*;
use bevy_renet::transport::*;
use bevy_renet::renet::transport::*;
use hashbrown::HashMap;
use piglog::prelude::*;
use clap::Parser;

use common::*;

#[derive(Resource, Deref, DerefMut)]
struct IPMap(HashMap<ClientId, SocketAddr>);

fn main() {
    let args = cli::Cli::parse();

    let mut app = App::new();

    app.add_plugins(MinimalPlugins);
    app.add_plugins(RenetServerPlugin);

    app.insert_resource(IPMap(HashMap::new()));
    app.insert_resource(args);

    create_server_and_transport(&mut app, args.ip, args.max);

    app.add_systems(Update, handle_messages_system);

    app.run();
}

fn handle_messages_system(
    mut server: ResMut<RenetServer>,
    mut ips: ResMut<IPMap>,
    transport: Res<NetcodeServerTransport>,
    args: Res<cli::Cli>,
) {
    while let Some(event) = server.get_event() {
        match event {
            ServerEvent::ClientConnected { client_id } => {
                ips.insert(client_id, transport.client_addr(client_id).unwrap());

                piglog::info!("Client '{}' joined!", ips.get(&client_id).unwrap());
            },
            ServerEvent::ClientDisconnected { client_id, reason } => {
                piglog::info!("Client '{}' left! (Reason: {reason})", ips.get(&client_id).unwrap());

                ips.remove(&client_id).unwrap();
            },
        };
    }

    for client_id in server.clients_id() {
        while let Some(message) = server.receive_message(client_id, DefaultChannel::ReliableOrdered) {
            let bytes = message.to_vec();
            let msg: ClientMessage = bincode::deserialize(&bytes).unwrap();

            if args.verbose {
                println!("<{}> {:?}", ips.get(&client_id).unwrap(), msg);
            }

            if msg == ClientMessage::Ping {
                piglog::info!("Got a ping from client!");

                server.send_message(
                    client_id,
                    DefaultChannel::ReliableOrdered,
                    bincode::serialize(&ServerMessage::Pong).unwrap(),
                );
            }
        }
    }
}

fn create_server_and_transport(app: &mut App, ip: SocketAddr, max: usize) {
    app.add_plugins(NetcodeServerPlugin);

    let server = RenetServer::new(ConnectionConfig::default());
    let socket = UdpSocket::bind(ip).unwrap();
    let server_config = ServerConfig {
        current_time: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap(),
        max_clients: max,
        protocol_id: PROTOCOL_ID,
        public_addresses: vec![ip],
        authentication: ServerAuthentication::Unsecure,
    };

    let transport = NetcodeServerTransport::new(server_config, socket).unwrap();

    app.insert_resource(server);
    app.insert_resource(transport);
}
