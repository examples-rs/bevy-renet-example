use clap::Parser;
use bevy::prelude::*;

#[derive(Parser, Resource, Clone, Copy)]
pub struct Cli {
    #[clap(long)]
    /// Server address (to listen on)
    pub ip: std::net::SocketAddr,

    #[clap(long)]
    /// Max amount of clients allowed to connect
    pub max: usize,

    #[clap(short, long)]
    /// Display messages
    pub verbose: bool,
}
