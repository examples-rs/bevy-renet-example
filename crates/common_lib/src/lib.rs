use serde::{Serialize, Deserialize};

/// Basically, the server only lets clients with this number join.
/// The server does this to stop any bugs caused by outdated clients.
pub const PROTOCOL_ID: u64 = 0;

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq)]
/// Messages that the client sends.
pub enum ClientMessage {
    Ping,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq)]
/// Messages that the server sends.
pub enum ServerMessage {
    Pong,
}
