use clap::Parser;
use bevy::prelude::*;

#[derive(Parser, Resource, Clone, Copy)]
pub struct Cli {
    #[clap(long)]
    /// Server address to connect to
    pub server: std::net::SocketAddr,

    #[clap(short, long)]
    /// Display messages
    pub verbose: bool,
}
