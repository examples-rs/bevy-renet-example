mod cli;

use std::net::*;
use std::time::*;
use bevy::prelude::*;
use bevy::log::LogPlugin;
use bevy_renet::*;
use bevy_renet::renet::*;
use bevy_renet::transport::*;
use bevy_renet::renet::transport::*;
use piglog::prelude::*;
use rand::Rng;
use clap::Parser;

use common::*;

fn main() {
    let args = cli::Cli::parse();

    let mut app = App::new();

    app.add_plugins(RenetClientPlugin);
    app.add_plugins(DefaultPlugins.set(
        WindowPlugin {
            primary_window: Some(Window {
                title: "Bevy Client".into(),
                ..default()
            }),
            ..default()
        }
    ).set(
        LogPlugin {
            filter: "wgpu_core=error,wgpu_hal=off,rechannel=warn".into(),
            level: bevy::log::Level::WARN,
        }
    ));

    app.insert_resource(ClearColor(Color::rgb(0.3, 0.3, 0.3)));
    app.insert_resource(args);

    create_client_and_transport(&mut app, args.server);

    app.add_systems(Startup, spawn_camera_system);
    app.add_systems(Update, handle_messages_system);
    app.add_systems(Update, send_ping_system);

    app.run();
}

fn spawn_camera_system(
    mut cmds: Commands,
) {
    cmds.spawn(
        Camera3dBundle {
            transform: Transform::from_xyz(5.0, 5.0, 5.0)
                .looking_at(Vec3::new(0.0, 0.0, 0.0), Vec3::Y),
            ..default()
        }
    );
}

fn panic_on_error_system(
    mut renet_error: EventReader<NetcodeTransportError>,
) {
    for e in renet_error.read() {
        panic!("{}", e);
    }
}

fn send_ping_system(
    mut client: ResMut<RenetClient>,
    input: Res<Input<KeyCode>>,
) {
    if input.just_pressed(KeyCode::Space) {
        if client.is_connected() {
            client.send_message(
                DefaultChannel::ReliableOrdered,
                bincode::serialize(&ClientMessage::Ping).unwrap(),
            );
        }
    }
}

fn handle_messages_system(
    mut client: ResMut<RenetClient>,
    args: Res<cli::Cli>,
) {
    if client.is_connected() {
        while let Some(message) = client.receive_message(DefaultChannel::ReliableOrdered) {
            let bytes = message.to_vec();
            let msg: ServerMessage = bincode::deserialize(&bytes).unwrap();

            if args.verbose {
                println!("<{}> {:?}", args.server, msg);
            }

            if msg == ServerMessage::Pong {
                piglog::info!("Got a pong from server!");
            }
        }
    }
}

fn create_client_and_transport(app: &mut App, server_ip: SocketAddr) {
    app.add_plugins(NetcodeClientPlugin);

    let client = RenetClient::new(ConnectionConfig::default());

    let socket = UdpSocket::bind("127.0.0.1:0").unwrap();
    let client_id: u64 = rand::thread_rng().gen_range(0..u64::MAX); // Generate a random u64 number.
    let current_time = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap();
    let authentication = ClientAuthentication::Unsecure {
        protocol_id: PROTOCOL_ID,
        user_data: None,
        server_addr: server_ip,
        client_id,
    };

    let transport = NetcodeClientTransport::new(current_time, authentication, socket).unwrap();

    app.insert_resource(client);
    app.insert_resource(transport);

    app.add_systems(Update, panic_on_error_system);
}
